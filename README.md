**Step 1** 

Add .aar Module to your project. 

**Step 2**

Add dependencies to /app/build.gradle

```
implementation('org.squirrelframework:squirrel-foundation:0.3.8') {
        exclude(group: 'javaassist', module: 'javassist')
        exclude(group: 'dom4j', module: 'dom4j')
        exclude(group: 'org.slf4j', module: 'slf4j-log4j12')
        exclude(group: 'log4j', module: 'log4j')
        exclude(group: 'org.mvel', module: 'mvel2')
        exclude(group: 'com.google.code.gson', module: 'gson')
    }
    implementation 'com.inrista.loggliest:loggliest:0.2'
    implementation 'com.github.bumptech.glide:glide:4.10.0'
    implementation 'com.loopj.android:android-async-http:1.4.9'
    implementation 'org.slf4j:slf4j-api:1.7.16'
    implementation 'com.github.tony19:logback-android-classic:1.1.1-3'
    implementation 'com.github.tony19:logback-android-core:1.1.1-3'
    implementation 'com.squareup.okhttp3:okhttp:4.5.0'
    implementation 'com.google.code.gson:gson:2.8.6'
    implementation 'com.android.support.constraint:constraint-layout:1.1.3'
    implementation 'com.google.firebase:firebase-messaging:17.3.4'
    implementation 'com.google.firebase:firebase-core:16.0.5'
    implementation 'com.otaliastudios:cameraview:1.5.0'
    implementation 'com.google.gms:google-services:4.2.0'
```



**Step 3**

Create a server config xml file. Name should be serverConfigWeb.xml 

Location : res/values/serverConfigWeb.xml

```xml
<resources>
<string name="server_config_web_address">PARTNER_URL</string>
<string name="server_config_web_auth_username"></string>
<string name="server_config_web_auth_password"></string>
</resources>
```

You have to change **PARTNER_URL** section with the address you will connect to the SDK. For testing you need to use [https://sdktest.11sight.com](https://sdktest.11sight.com/) and for *Production* you need to use [https://sdk.11sight.com](https://sdk.11sight.com/).

If you want to define a custom URL address, please contact our support on how to do this.

**Step 4**

Initilize 11Sight SDK from Application class. 

```kotlin
class DemoApplication : Application(), IISightSDKManager.ICallback {

    private lateinit var sdkManager: IISightSDKManager

    override fun onCreate() {
        super.onCreate()
				
      	// Loggly should use with empty token and empty log url. Loggly will not work this way.
        Loggly.with(this, "")
            .appendDefaultInfo(true)
            .uploadIntervalLogCount(5)
            .uploadIntervalSecs(5)
            .maxSizeOnDisk(500000)
            .init()

        IISightSDKManager.build(this)
        sdkManager = IISightSDKManager.getInstance(this)

    }

    fun getSDKManager(): IISightSDKManager = sdkManager

    override fun process(p0: Any?) {
        val intent = Intent(applicationContext, Activity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        applicationContext.startActivity(intent)
    }
}
```



**Step 5** 

Set application properties from AndroidManifest.xml. Application tag looks like:

```xml
 <application
 				//allow allowBackup should set to false
        android:allowBackup="false"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:name="com.postandroidxdemo.DemoApplication"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme"
        // replace should set to android:theme
        tools:replace="android:theme">
```



**Note**

11Sight SDK uses gradle 3.3.0. If you are using newer version than 3.3.0, you should add **gradle.properties** to

```
android.useNewApkCreator=false
```



**Step 6**

11Sight SDK uses Firebase Messaging Service for notification service. You need to add google-services.json and google-service plugin

Root build.gradle

```
buildscript {
    ext.kotlin_version = '1.3.61'
    repositories {
        google()
        jcenter()
        
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.6.2'
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
        // You should use 4.3.3
        classpath 'com.google.gms:google-services:4.3.3'
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}
```



If your application uses Push Notifications, there is a service class which can help you to differentiate whether received notification belongs to SDK or not. 

```kotlin
class DemoFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        super.onMessageReceived(remoteMessage)

        val check = (application as).getSDKManager().isIISightNotification(remoteMessage)
            if (check) { // Handle 11Sight notification
                 (application as DemoApplication).
              getSDKManager().handleIISightNotification(remoteMessage)
            } else {
                // Handle message from other servers
            }

    }

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
    }

}
```

**Important:** *handleIISightNotification()* method handles incoming calls and have to be implemented.

Also add service tag to AndroidManifest.xml 

Final version of AndroidManifest.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    package="com.postandroidxdemo">

    <application
        android:allowBackup="false"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:name="com.postandroidxdemo.DemoApplication"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme"
        tools:replace="android:theme">
        <activity android:name="com.postandroidxdemo.MainActivity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>

        <service
            android:name=".DemoFirebaseMessagingService"
            android:enabled="true"
            android:exported="true">
            <intent-filter>
                <action android:name="com.google.firebase.MESSAGING_EVENT" />
            </intent-filter>
        </service>
    </application>

</manifest>
```



**Step 7**

Go to SDK web panel.( *[https://sdk.11sight.com](https://sdk.11sight.com/)* for *Production* and *[https://sdktest.11sight.com](https://sdktest.11sight.com/)* for *Sandbox*.)

Select your organization.

Set FCM token. (From your Firebase Project.)

Set Android Application ID

**Android 10 Receiving Calls**

When app is in background you need to add overlay permission for Android 10. 

```kotlin
(application as DemoApplication).getSDKManager().checkOverlayPermissionDialog(this)
```

**Theme Fix**

If you are using a action bar theme for your app. You need to add these to app's theme. 

styles.xml

```xml
<item name="windowActionBar">false</item>
<item name="windowNoTitle">true</item>
```



**Login User**

```kotlin
 (application as DemoApplication).getSDKManager()
            .loginUser("mail", "password", this, {
                // Success
            }, {
                // Fail
            })
```



**Logout User**

```kotlin
 (application as DemoApplication).getSDKManager()
            .logoutUser(  {
                // Success
            }, {
                // Fail
            })
```



**Session** 

You can access some user data from Session object. For example

```kotlin
Session.getInstance().userId
Session.getInstance().userEmail
```

**Make a call**

You can make a call with 3 types:

OutgoingCallType.VIDEO - OutgoingCallType.AUDIO - OutgoingCallType.CHAT

Video call example: 

```kotlin
(application as DemoApplication).
getSDKManager().makeCall(context,OutgoingCallType.VIDEO,"button_id")
```

Button id is unique id for each 11Sight users button. You can consume it from our REST API. Documentation is 

[here]: https://www.11sight.com/sdk/docs/api-reference/.

**Receive a call**

You don't need to anything special for receiving a call.

Tip: If you can make a call but there is no receiving call, check firebase integration part.



**Listen Call Events**

```java
Session.getInstance().setActiveCallListener(new ActiveCallListener() {
    @Override
    public void onCallStarted(String callId) {
        Toast.makeText(context,"Call Started, callId:  " + callId,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCallEnded() {
        Toast.makeText(context,"Call Ended",Toast.LENGTH_LONG).show();
    }
});
```



