package com.postandroidxdemo

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage



class DemoFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        val check = (application as DemoApplication).getSDKManager().isIISightNotification(p0)
            if (check) { // Handle 11Sight notification
                 (application as DemoApplication).getSDKManager().handleIISightNotification(p0)
            } else {
                // Handle message from other servers
            }

    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }


}