package com.postandroidxdemo

import android.app.Activity
import android.app.Application
import android.content.Intent
import com.elevensight.sdk.sdk.IISightSDKManager
import com.inrista.loggliest.Loggly


class DemoApplication : Application(), IISightSDKManager.ICallback {

    private lateinit var sdkManager: IISightSDKManager

    override fun onCreate() {
        super.onCreate()

        Loggly.with(this, "")
            .appendDefaultInfo(true)
            .uploadIntervalLogCount(5)
            .uploadIntervalSecs(5)
            .maxSizeOnDisk(500000)
            .init()

        IISightSDKManager.build(this)
        sdkManager = IISightSDKManager.getInstance(this)

    }

    fun getSDKManager(): IISightSDKManager = sdkManager

    override fun process(p0: Any?) {
        val intent = Intent(applicationContext, Activity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        applicationContext.startActivity(intent)
    }
}