package com.postandroidxdemo

import android.os.Bundle

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.elevensight.sdk.models.OutgoingCallType
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        //(application as DemoApplication).getSDKManager().checkOverlayPermissionDialog(this)
    }

    //Change Button Id
    private val buttonId = "KjVEHjL_DAtATpbOsTk30rwBeGJmLYWwaHvJwDEKMZDp1PYhJc"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        buttonCall.setOnClickListener {
            (application as DemoApplication).getSDKManager().makeCall(
                this,
                OutgoingCallType.VIDEO,
                buttonId
            )
        }

    }
}

