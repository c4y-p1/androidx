package com.postandroidxdemo.ui.login

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.postandroidxdemo.DemoApplication
import com.postandroidxdemo.MainActivity
import com.postandroidxdemo.R
import kotlinx.android.synthetic.main.activity_demo_login.*
import kotlinx.android.synthetic.main.activity_main.*

class DemoLoginActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
       // (application as DemoApplication).getSDKManager().checkOverlayPermissionDialog(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo_login)

        loginBtn.setOnClickListener(View.OnClickListener {
            if (TextUtils.isEmpty(username.text.toString())) {
                Toast.makeText(
                    this@DemoLoginActivity,
                    "Please enter valid email",
                    Toast.LENGTH_LONG
                ).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(password.text.toString())) {
                Toast.makeText(this@DemoLoginActivity, "Please enter password", Toast.LENGTH_LONG)
                    .show()
                return@OnClickListener
            }
            loading.visibility = VISIBLE
            (application as DemoApplication).getSDKManager()
                .loginUser(username.text.toString(), password.text.toString(), this, {
                    loading.visibility = GONE
                    Toast.makeText(this@DemoLoginActivity, "Signed in", Toast.LENGTH_LONG).show()
                    startActivity(Intent(this@DemoLoginActivity, MainActivity::class.java))
                    finish()
                }, {
                    loading.visibility = GONE
                    Toast.makeText(this@DemoLoginActivity, "Error", Toast.LENGTH_LONG).show()
                })


        })


    }
}
